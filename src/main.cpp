#include <iostream>
using namespace std;

#include "../include/DynamicContainer.h"
#include "../src/DynamicContainer.cpp"

int main() {
    cargo::DynamicContainer<int> dynamicContainer1(10);
    cout << dynamicContainer1 << endl;

    cargo::DynamicContainer<int> dynamicContainer2(10, 5);
    dynamicContainer2[0] = 2;
    cout << dynamicContainer2 << endl;
    cout << dynamicContainer2.getSize() << endl;

    //dynamicContainer2.resize(13);
    //cout << dynamicContainer2 << endl;

    cargo::DynamicContainer<int> dynamicContainer3(10, 5);
    cout << dynamicContainer3 << endl;

    if (dynamicContainer2 != dynamicContainer3){
        cout << 1 << endl;
    } else {
        cout << 0 << endl;
    }

    if (dynamicContainer2 >= dynamicContainer3){
        cout << 1 << endl;
    } else {
        cout << 0 << endl;
    }

    dynamicContainer2+dynamicContainer3;
    cout << dynamicContainer2 << endl;

    return 0;
}