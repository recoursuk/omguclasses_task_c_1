//
// Created by asbox on 05.04.2020.
//

#ifndef TASK1_DYNAMICCONTAINER_H
#define TASK1_DYNAMICCONTAINER_H

#pragma once

#include <iostream>

namespace cargo {
    template <class U>
    class DynamicContainer {
        U *array;
        size_t length = 0;

        void setLength(int newLength) {
            this->length = newLength;
        }

    public:
        //выделяется память на 10 элементов класса
        DynamicContainer();

        //выделяется память на size элементов
        DynamicContainer(int size);

        //выделяется память на size элементов и дается дефолтное значение каждого элемента
        DynamicContainer(int size, U defaultValue);

        DynamicContainer(DynamicContainer<U> const &other);

        DynamicContainer<U> &operator=(DynamicContainer<U> const &other);

        ~DynamicContainer();

        int getSize() const;

        U &operator[](int i);

        void resize(int newsize);

        bool operator==(const DynamicContainer &other) const;

        bool operator!=(const DynamicContainer &other) const;

        bool operator<(const DynamicContainer &other) const;

        bool operator>=(const DynamicContainer &other) const;

        bool operator>(const DynamicContainer &other) const;

        bool operator<=(const DynamicContainer &other) const;

        void operator+(const DynamicContainer &other);

        friend std::ostream &operator<<(std::ostream &os, const DynamicContainer &other) {
            os << "size:" << other.getSize() << std::endl;
            for (int i = 0; i < other.getSize(); ++i) {
                os << other.array[i] << " ";
            }
            return os;
        }

        friend std::istream &operator>>(std::istream &is, const DynamicContainer &other) {
            is >> other.setLength();
            for (int i = 0; i < other.getSize(); i++) {
                is >> other.array[i];
            }
            return is;
        }

        DynamicContainer(DynamicContainer &&other);

        DynamicContainer &operator=(DynamicContainer &&other);
    };
}

#endif //TASK1_DYNAMICCONTAINER_H
