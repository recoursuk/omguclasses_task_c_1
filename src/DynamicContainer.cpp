//
// Created by asbox on 05.04.2020.
//

#include "../include/DynamicContainer.h"
#include <iostream>
using namespace std;

//#define int DEFFAULT_SIZE = 10;

//выделяется память на 10 элементов класса

template<class U>
cargo::DynamicContainer<U>::DynamicContainer() {
    DynamicContainer(10, U());
}

//выделяется память на size элементов
template<class U>
cargo::DynamicContainer<U>::DynamicContainer(int size) {
    DynamicContainer(size, U());
}

//выделяется память на size элементов и дается дефолтное значение каждого элемента
template<class U>
cargo::DynamicContainer<U>::DynamicContainer(int size, U defaultValue) {
    this->array = new U[size];
    setLength(size);
    for (int i = 0; i < this->length; ++i) {
        array[i] = defaultValue;
    }
}

template<class U>
cargo::DynamicContainer<U>::DynamicContainer(DynamicContainer<U> const &other) {
    ~DynamicContainer();
    DynamicContainer(other.length);
    for (int i = 0; i < length; ++i) {
        array[i] = other.array[i];
    }
}

template<class U>
cargo::DynamicContainer<U> &cargo::DynamicContainer<U>::operator=(const cargo::DynamicContainer<U> &other) {
    if (this == &other) {
        return this;
    }
    return DynamicContainer(other);
}

template<class U>
cargo::DynamicContainer<U>::~DynamicContainer() {
    delete [] array;
}

template<class U>
int cargo::DynamicContainer<U>::getSize() const {
    return length;
}

template<class U>
U &cargo::DynamicContainer<U>::operator[](int i) {
    return array[i];
}

template<class U>
void cargo::DynamicContainer<U>::resize(int newsize) {
    U *tmp = new U[newsize];
    if (newsize > length) {
        for (int i = 0; i < length; i++){
            tmp[i] = array[i];
        }
        for (int i = length; i < newsize; i++){
            tmp[i] = 0;
        }
    } else if(newsize < length) {
        for(int i = 0; i < newsize; i++){
            tmp[i] = array[i];
        }
    } else {
        return;
    }
    length = newsize;
    delete []array;
    array = tmp;
}

template<class U>
bool cargo::DynamicContainer<U>::operator==(const cargo::DynamicContainer<U> &other) const {
    if(other.getSize() != length){
        throw std::logic_error("Different sizes");
    }
    for (int i = 0; i < length; i++){
        if(other.array[i] != array[i]){
            return false;
        }
    }
    return true;
}

template<class U>
bool cargo::DynamicContainer<U>::operator!=(const cargo::DynamicContainer<U> &other) const {
    if(other.getSize() != length){
        throw std::logic_error("Different sizes");
    }
    for (int i = 0; i < length; i++){
        if(other.array[i] != array[i]){
            return true;
        }
    }
    return false;
}


template<class U>
bool cargo::DynamicContainer<U>::operator<(const cargo::DynamicContainer<U> &other) const {
    int cycleLength = 0;
    if(length <= other.getSize()){
        cycleLength = length;
    } else {
        cycleLength = other.getSize();
    }

    for(int i = 0; i < cycleLength; i++){
        if (array[i] >= other.array[i]) {
            return false;
        }
    }
    return true;
}

template<class U>
bool cargo::DynamicContainer<U>::operator>=(const cargo::DynamicContainer<U> &other) const {
    int cycleLength = 0;
    if(length <= other.getSize()){
        cycleLength = length;
    } else {
        cycleLength = other.getSize();
    }

    for(int i = 0; i < cycleLength; i++){
        if (array[i] >= other.array[i]) {
            return true;
        }
    }
    return false;
}

template<class U>
bool cargo::DynamicContainer<U>::operator>(const cargo::DynamicContainer<U> &other) const {
    int cycleLength = 0;
    if(length <= other.getSize()){
        cycleLength = length;
    } else {
        cycleLength = other.getSize();
    }

    for(int i = 0; i < cycleLength; i++){
        if (array[i] <= other.array[i]) {
            return false;
        }
    }
    return true;
}

template<class U>
bool cargo::DynamicContainer<U>::operator<=(const cargo::DynamicContainer<U> &other) const {
    int cycleLength = 0;
    if(length <= other.getSize()){
        cycleLength = length;
    } else {
        cycleLength = other.getSize();
    }

    for(int i = 0; i < cycleLength; i++){
        if (array[i] <= other.array[i]) {
            return true;
        }
    }
    return false;
}

template<class U>
void cargo::DynamicContainer<U>::operator+(const cargo::DynamicContainer<U> &other) {
    int tmp = length;
    resize(length+other.getSize());
    for(int i = tmp; i < length + other.getSize(); i++){
        array[i] = other.array[i-tmp];
    }
}

template<class U>
cargo::DynamicContainer<U>::DynamicContainer(cargo::DynamicContainer<U> &&other){
    this->array = other.array;
    this->length = other.length;
    other.array = nullptr;
}

template<class U>
cargo::DynamicContainer<U> &cargo::DynamicContainer<U>::operator=(cargo::DynamicContainer<U> &&other) {
    if(&other == this)
        return this;
    delete [] array;
    this->array = other.array;
    other.array = nullptr;
    return *this;
}