cmake_minimum_required(VERSION 3.15)
project(task1)

set(CMAKE_CXX_STANDARD 14)

add_executable(task1 src/main.cpp src/DynamicContainer.cpp include/DynamicContainer.h)